import { Component, OnInit } from '@angular/core';
import { HubConnection } from '@aspnet/signalr';

@Component({
    selector: 'app-home-component',
    templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit {
    public async: any;
    message = '';
    messages: string[] = [];

    constructor() {
    }

    public sendMessage(): void {
        const data = `Sent: ${this.message}`;

        //this._hubConnection.invoke('Send', data);
        this.messages.push(data);
    }

    ngOnInit() {
        let hubConnection = new HubConnection('http://localhost:60331/signalr/hubs/chat');

        hubConnection.on('Send', (data: any) => {
            const received = `Received: ${data}`;
            this.messages.push(received);
        });

        hubConnection.start()
            .then(() => {
                console.log('Hub connection started')
            })
            .catch((error: any) => {
                console.log('Hub error -> ' + error);
            });
    }

}