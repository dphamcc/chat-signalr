﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace ChatHub.Hubs
{
    [HubName("chat")]
    public class ChatHub : Hub
    {
        private readonly AlertBroadcaster _alertBroadcaster;

        public ChatHub() : this(AlertBroadcaster.Instance)
        {

        }

        public ChatHub(AlertBroadcaster alertBroadcaster)
        {
            _alertBroadcaster = alertBroadcaster;
        }

        public void Send(string name, string message)
        {
            var loggedInUsername = Context.User.Identity.Name;
            Clients.All.addNewMessageToPage(loggedInUsername, message);
        }

        public void SendChatMessage(string who, string message)
        {
            string name = Context.User.Identity.Name;

            Clients.Group(who).addNewMessageToPage(name, message);
        }

        public override Task OnConnected()
        {
            string name = Context.User.Identity.Name;

            Groups.Add(Context.ConnectionId, name);

            return base.OnConnected();
        }
    }
}