﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Configuration;
using System.IO;
using TableDependency;
using TableDependency.Enums;
using TableDependency.EventArgs;
using TableDependency.SqlClient;

namespace ChatHub.Hubs
{
    public class AlertBroadcaster
    {
        private readonly static Lazy<AlertBroadcaster> _instance = new Lazy<AlertBroadcaster>(
            () => new AlertBroadcaster(GlobalHost.ConnectionManager.GetHubContext<ChatHub>().Clients));

        private static SqlTableDependency<Alert> _tableDependency;

        private AlertBroadcaster(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;

            var mapper = new ModelToTableMapper<Alert>();
            mapper.AddMapping(a => a.Email, "Email");
            mapper.AddMapping(a => a.Message, "Message");

            _tableDependency = new SqlTableDependency<Alert>(
                ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString,
                "Alerts",
                mapper);

            _tableDependency.OnChanged += SqlTableDependency_Changed;
            _tableDependency.OnError += SqlTableDependency_OnError;
            _tableDependency.Start();
        }

        public static AlertBroadcaster Instance {
            get
            {
                return _instance.Value;
            }
        }

        private IHubConnectionContext<dynamic> Clients { get; set; }

        void SqlTableDependency_OnError(object sender, TableDependency.EventArgs.ErrorEventArgs e)
        {
            throw e.Error;
        }

        void SqlTableDependency_Changed(object sender, RecordChangedEventArgs<Alert> e)
        {
            if (e.ChangeType != ChangeType.None)
            {
                Clients.Group(e.Entity.Email).addNewMessageToPage("System", e.Entity.Message);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // to detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _tableDependency.Stop();
                }

                disposedValue = true;
            }
        }

        ~AlertBroadcaster()
        {
            Dispose(false);
        }

        // This code added to correctly implement the dispoable pattern
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}